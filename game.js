//Game field
const gameField = document.getElementById('gameField');

//Buttons
const buttonStart = document.getElementById('buttonStart');
const buttonOneMatch = document.getElementById('buttonOneMatch');
const buttonTwoMatch = document.getElementById('buttonTwoMatch');
const buttonThreeMatch = document.getElementById('buttonThreeMatch');

//Settings
const modeComputerFirst = document.getElementById('modeComputerFirst');
const amountOfMatches = document.getElementById('amountOfMatches');

// Score
const myScore = document.getElementById('myScore');
const computerScore = document.getElementById('computerScore');
const totalScore = document.getElementById('totalScore');
let numberOfMyScore = 0;
let numberOfComputerScore = 0;

// For game field
const match = `<div class="match">&#x1F58D;</div>`;
let matches = [];


// Elements for navigation


buttonStart.onclick = startOfGame;

// Functions of Buttons
buttonOneMatch.onclick = () => yourTurn(1);
buttonTwoMatch.onclick = () => yourTurn(2);
buttonThreeMatch.onclick = () => yourTurn(3);


//Start of game
function startOfGame() {
    matches.splice(0, matches.length);
    numberOfMyScore = 0;
    numberOfComputerScore = 0;
    createGameField(2 * amountOfMatches.value + 1);
    showScore();
    if (modeComputerFirst.checked) {
        computerTurn();
        showScore();
    }
}


//Functions for gaming
function strategicComputer() {
    if (amountOfMatches.value % 4 === 0) {
        if (((matches.length - 1) % 4 === 0) || ((matches.length - 1) % 4 === 1)) {
            return 1
        }
        if (((matches.length - 3) % 4 === 0) || ((matches.length - 3) % 4 === 1)) {
            return 3
        }
    } else {
        if (matches.length % 4 === 0) {
            return 1
        }
        if (matches.length >= 5) {
            return matches.length % 4
        }
        if (matches.length < 5) {
            switch (matches.length) {
                case 3:
                    if (numberOfComputerScore % 2 === 0) {
                        return 2
                    } else {
                        return 3
                    }
                case 2:
                    if (numberOfComputerScore % 2 === 0) {
                        return 2
                    } else {
                        return 1
                    }
                case 1:
                    return 1
            }
        } else if ((matches.length === 3) && (computerScore % 2 === 0)) {
            return 2
        } else if ((matches.length === 2) && (computerScore % 2 === 0)) {
            return 2
        } else if ((matches.length === 2) && (computerScore % 2 === 1)) {
            return 1
        } else {
            return matches.length % 4
        }
    }
}

function createGameField(amountOfMatches){
    for(let i = 1; i <= amountOfMatches; i++){
        matches.push(match)
    }
    gameField.innerHTML = matches.join('');
}

function showScore(){
  myScore.innerHTML = `
  ${numberOfMyScore}
  `;
    computerScore.innerHTML = `
  ${numberOfComputerScore}
  `;
    totalScore.innerHTML = `
  ${matches.length} &nbsp &#x1F58D;
  `;
}

function yourTurn(numberOfTakenMatch) {
    if (matches.length - numberOfTakenMatch >= 0) {
            takeMatch(numberOfTakenMatch);
            numberOfMyScore += numberOfTakenMatch;
            showScore();
            checkWin();
            computerTurn();
    } else {
        alert(`There's no enough matches! Take a less amount, please!`)
    }
}

function takeMatch(numberOfTakenMatch) {
    matches.splice(matches.length - numberOfTakenMatch, numberOfTakenMatch);
    gameField.innerHTML = matches.join('');
}

function computerTurn() {
    if (matches.length > 0) {
        let numberOfTakenMatch = strategicComputer();
        takeMatch(numberOfTakenMatch);
        numberOfComputerScore += numberOfTakenMatch;
        showScore();
    }
        checkWin();
}

function checkWin() {
    if(matches.length === 0) {
        if(numberOfMyScore % 2 === 0){
           alert('You have won!');
        }
        if(numberOfComputerScore % 2 === 0){
            alert('Computer has won!');
        }
    }
}

